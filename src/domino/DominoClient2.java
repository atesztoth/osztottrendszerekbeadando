package domino;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.exit;

public class DominoClient2 extends DominoClient {

    private String rmiServiceName;
    private String userName;
    private int steps = 0;
    private boolean messageUJsent = false;
    private int saveAfterStep;
    DominoStorageIface dominoStorage;
    DominoConfigProvider dominoConfigProvider = DominoConfigProvider.getInstance();

    public static void main(String[] args) {
        DominoClient2 dominoClient2 = null;
        if (args.length == 1) {
            dominoClient2 = new DominoClient2(args[0]);
        } else {
            int kicsekk = Integer.valueOf(args[1]);
            dominoClient2 = new DominoClient2(args[0], kicsekk, args[2], args[3]);
        }

        dominoClient2.connectToServer();
    }

    public DominoClient2(String myName) {
        super(myName);
    }

    public DominoClient2(String myName, int saveAfterStep, String dominoStorageName, String userName) {
        super(myName);
        this.rmiServiceName = dominoStorageName;
        this.userName = userName;
        this.saveAfterStep = saveAfterStep;

        try {
            startRMILogic();
        } catch (RemoteException e) {
            System.out.println("Error during communication with the server.");
            e.printStackTrace();
        } catch (NotBoundException e) {
            System.out.println("There is no such object bound to this key.");
            e.printStackTrace();
        }
    }

    /**
     * This method start communication with the server.
     */
    private void startRMILogic() throws RemoteException, NotBoundException {
        // fetch dominos from server
        // First, get the stub we can use for communication with the server.
        Registry registry = LocateRegistry.getRegistry("localhost", dominoConfigProvider.getIntValueOf("rmi_server_port"));
        // getting the stub object:
        dominoStorage = (DominoStorageIface) registry.lookup(rmiServiceName);

        // aaaand we should load dominos
        ArrayList<String> dominoStrings = (ArrayList<String>) dominoStorage.load(userName);
        for (String dominoString : dominoStrings) {
            dominos.add(new Domino(dominoString));
            System.err.println(dominoString);
        }
    }

    /**
     * Handles domino storing that the server has given us. This is especially the saving part.
     *
     * @param individualDominos
     */
    @Override
    protected void storeDominosFromServer(String[] individualDominos) {
        if (dominos.size() >= 7) {
            return; // We're done.
        }

        int index = 0;
        while (dominos.size() < 7) {
            dominos.add(new Domino(individualDominos[index]));
            ++index;
        }
    }

    /**
     * Not only sends the message, but ticks a flag as well.
     */
    @Override
    protected void sendNewDominoMessage() {
        super.sendNewDominoMessage();
        messageUJsent = true;
    }

    // I know its ugly to be calling a method like this, but it works so I will just leave it like this.
    @Override
    protected void notifyClient2() {
        steps += 1;

        if (steps != saveAfterStep) {
            return;
        }

        // creating a list to send:
        List<String> marshallMatters = new ArrayList<>(); // Joke hidden here :)
        for (Domino d : dominos) {
            marshallMatters.add(d.convertToText());
        }

        try {
            dominoStorage.save(userName, marshallMatters);
        } catch (RemoteException e) {
            System.err.println("Remote Error. Disconnecting, exiting.");
            e.printStackTrace();
            exit(1);
        }
    }
}