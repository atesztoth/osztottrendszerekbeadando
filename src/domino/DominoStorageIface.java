package domino;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by atesztoth on 2017. 05. 23..
 */
public interface DominoStorageIface extends Remote {
    void save(String userName, List<String> dominosOfUser) throws RemoteException;

    List<String> load(String userName) throws RemoteException;
}
