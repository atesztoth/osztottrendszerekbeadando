package domino.server;

import domino.DominoConfigProvider;
import domino.DominoStorageIface;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class DominoDeploy {

    public static void deploy(Registry registry, String rmiServiceName, String dbname) throws RemoteException {
        DominoStorageImpl dominoStorage = new DominoStorageImpl(dbname);
        // let's create the stub, the proxy object
        // DominoStorageIface dominoStorageStub = (DominoStorageIface) UnicastRemoteObject.exportObject(dominoStorage, port);
        registry.rebind(rmiServiceName, dominoStorage); // rebind won't throw an error if there is an object already binded
    }

    public static void main(String[] args) {
        try {
            DominoConfigProvider dominoConfigProvider = DominoConfigProvider.getInstance();
            final int port = dominoConfigProvider.getIntValueOf("rmi_server_port");
            Registry registry = LocateRegistry.createRegistry(port);
            deploy(registry, "dominoStorage", "dominoDb");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
