package domino.server;

import domino.Domino;
import domino.DominoStorageIface;
import domino.Interface.ServerInterface;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.exit;

public class DominoStorageImpl extends UnicastRemoteObject implements DominoStorageIface {

    private Connection connection;

    public DominoStorageImpl(String text) throws RemoteException {
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            connection = DriverManager.getConnection("jdbc:hsqldb:file:" + text);
            // If we got a connection, let's handle table managing:
            Statement statement = connection.createStatement();
//            PreparedStatement removeTable = connection.prepareStatement("DROP TABLE IF EXISTS Dominos");
//            PreparedStatement createTable = connection.prepareStatement(
//                    "CREATE TABLE Dominos (" +
//                            "   idx INTEGER NOT NULL, " +
//                            "   usr VARCHAR(50) NOT NULL, " +
//                            "   domino VARCHAR(5) NOT NULL, " +
//                            ");");
//
//            removeTable.executeUpdate();
//            createTable.executeUpdate();
            statement.executeUpdate("DROP TABLE IF EXISTS Dominos;");
            statement.executeUpdate("CREATE TABLE Dominos (" +
                    "idx INTEGER NOT NULL," +
                    " usr VARCHAR(50) NOT NULL," +
                    " domino VARCHAR(5) NOT NULL);");
        } catch (SQLException e) {
            e.printStackTrace();
            exit(1);
        } catch (ClassNotFoundException e) {
            System.out.println("Probably could not load HLSQL DB Driver.");
            e.printStackTrace();
            exit(1);
        }
    }

    @Override
    public synchronized void save(String userName, List<String> dominosOfUser) {
        // remove all the rows related to userName
        try {
            connection.nativeSQL("DELETE FROM Dominos WHERE usr = '" + userName + "'");
        } catch (SQLException e) {
            System.out.println("Failed to delete rows from Dominos.");
            e.printStackTrace();
            exit(1);
        }

        // Inserting:
        try {
            PreparedStatement insertionStatement = connection.prepareStatement(
                    "INSERT INTO Dominos (idx, usr, domino) VALUES (?, ?, ?)"
            );

            for (int i = 0; i < dominosOfUser.size(); i++) {
                insertionStatement.setInt(1, (i + 1));
                insertionStatement.setString(2, userName);
                insertionStatement.setString(3, dominosOfUser.get(i));

                // inserting domino into table
                insertionStatement.executeUpdate();
            }
        } catch (SQLException e) {
            System.out.println("SQL Syntax error.");
            e.printStackTrace();
            exit(1);
        }
    }

    @Override
    public synchronized List<String> load(String userName) {
        List<String> ayyLMAO = new ArrayList<>();

        try {
            Statement getDominos = connection.createStatement();
            ResultSet resultSet = getDominos.executeQuery("SELECT * FROM Dominos WHERE usr = '" + userName + "'");

            while (resultSet.next()) {
                ayyLMAO.add(resultSet.getString("domino"));
            }

            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
            exit(1);
        }

        return ayyLMAO;
    }
}
